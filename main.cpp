#include <iostream>
#include <cstdio>
#include "getcb.h"

typedef void (cb_t)(uint8_t, uint8_t);

void native_fun(cb_t *cb, uint8_t a, uint8_t b) {
	cb(a, b);
}

class Foo {
	private:
		int n;
		void cb(uint8_t x, uint8_t y) {
			printf("Foo::cb(%hhd, %hhd)\n", x, y);
			printf("Foo::n eq %d\n", this->n);
		}
	public:
		Foo(int m) {
			n = m;
			cb_t *f = GETCB(cb_t, Foo)
				(std::bind(&Foo::cb, this, std::placeholders::_1, std::placeholders::_2));
			native_fun(f, 3, 4);
		}
};

int main() {
	Foo foo1(1);
	Foo foo2(2);
	exit(0);
}
